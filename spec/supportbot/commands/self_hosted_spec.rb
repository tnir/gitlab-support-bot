require 'spec_helper'

describe SupportBot::Commands::SelfHosted do
  # Record this once because the number of tickets will keep changing otherwise.
  vcr_options = { cassette_name: 'zendesk_tickets' }

  def app
    SupportBot::Bot.instance
  end

  subject { app }

  it_behaves_like 'a slack ruby bot'

  context 'the commands', vcr: vcr_options do
    it 'returns self-hosted tickets' do
      expected_responses.each do |response|
        expect(message: "#{SlackRubyBot.config.user} self-hosted", channel: 'channel').to respond_with_slack_message(response)
      end
    end

    it 'returns sh tickets' do
      expected_responses.each do |response|
        expect(message: "#{SlackRubyBot.config.user} sh", channel: 'channel').to respond_with_slack_message(response)
      end
    end

    it 'returns self-managed tickets' do
      expected_responses.each do |response|
        expect(message: "#{SlackRubyBot.config.user} self-managed", channel: 'channel').to respond_with_slack_message(response)
      end
    end

    it 'returns sm tickets' do
      expected_responses.each do |response|
        expect(message: "#{SlackRubyBot.config.user} sm", channel: 'channel').to respond_with_slack_message(response)
      end
    end
  end

  def expected_responses
    triage = ticket_counts[:need_org_triage]
    premium = ticket_counts[:sm_premium_ultimate]
    starter = ticket_counts[:sm_starter_free]
    total = triage + premium + starter

    [
      /Needs Org & Triage: \*#{triage}\*\n/,
      /Self-Managed Premium & Ultimate: \*#{premium}\*\n/,
      /Self-Managed Starter & Free: \*#{starter}\*\n/,
      /Total: \*#{total}\*/
    ]
  end
end
