require 'spec_helper'

describe SupportBot::Commands::LicenseRenewals do
  # Record this once because the number of tickets will keep changing otherwise.
  vcr_options = { cassette_name: 'zendesk_tickets' }

  def app
    SupportBot::Bot.instance
  end

  subject { app }

  it_behaves_like 'a slack ruby bot'

  context 'the commands', vcr: vcr_options do
    it 'returns license&renewals tickets' do
      expect(message: "#{SlackRubyBot.config.user} license-renewals", channel: 'channel').to respond_with_slack_message(expected_response)
    end

    it 'returns lr tickets' do
      expect(message: "#{SlackRubyBot.config.user} lr", channel: 'channel').to respond_with_slack_message(expected_response)
    end
  end

  def expected_response
    lr = ticket_counts[:upgrade_renewals_ar]

    /Upgrades, Renewals & AR: \*#{lr}\*/
  end
end
