require 'spec_helper'

describe SupportBot::Commands::Services do
  # Record this once because the number of tickets will keep changing otherwise.
  vcr_options = { cassette_name: 'zendesk_tickets' }

  def app
    SupportBot::Bot.instance
  end

  subject { app }

  it_behaves_like 'a slack ruby bot'

  context 'the commands', vcr: vcr_options do
    it 'returns services tickets' do
      expected_responses.each do |response|
        expect(message: "#{SlackRubyBot.config.user} services", channel: 'channel').to respond_with_slack_message(response)
      end
    end

    it 'returns s tickets' do
      expected_responses.each do |response|
        expect(message: "#{SlackRubyBot.config.user} s", channel: 'channel').to respond_with_slack_message(response)
      end
    end
  end

  def expected_responses
    triage = ticket_counts[:need_org_triage]
    subscribers = ticket_counts[:dot_com_subscribers]
    trials = ticket_counts[:dot_com_trials]
    free = ticket_counts[:dot_com_free]
    total = triage + subscribers + trials + free

    [
      /Needs Org & Triage: \*#{triage}\*\n/,
      /GitLab.com Subscribers: \*#{subscribers}\*\n/,
      /GitLab Trials: \*#{trials}\*\n/,
      /GitLab Free: \*#{free}\*\n/,
      /Total: \*#{total}\*/
    ]
  end
end
