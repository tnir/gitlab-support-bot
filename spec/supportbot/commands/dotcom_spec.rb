require 'spec_helper'

describe SupportBot::Commands::Dotcom do
  # Record this once because the number of tickets will keep changing otherwise.
  vcr_options = { cassette_name: 'zendesk_tickets' }

  def app
    SupportBot::Bot.instance
  end

  subject { app }

  it_behaves_like 'a slack ruby bot'

  context 'the commands', vcr: vcr_options do
    it 'returns dotcom tickets' do
      expect(message: "#{SlackRubyBot.config.user} dotcom", channel: 'channel').to respond_with_slack_message(expected_response)
    end

    it 'returns dc tickets' do
      expect(message: "#{SlackRubyBot.config.user} dc", channel: 'channel').to respond_with_slack_message(expected_response)
    end
  end

  def expected_response
    subscribers = ticket_counts[:dot_com_subscribers]
    trials = ticket_counts[:dot_com_trials]
    free = ticket_counts[:dot_com_free]
    total = subscribers + trials + free

    /GitLab.com Subscribers: \*#{subscribers}\*\nGitLab Trials: \*#{trials}\*\nGitLab Free: \*#{free}\*\nTotal: \*#{total}\*/
  end
end
