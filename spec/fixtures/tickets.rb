TICKET_COUNTS = {
  need_org_triage: 20,
  dot_com_subscribers: 16,
  dot_com_trials: 5,
  dot_com_free: 23,
  sm_premium_ultimate: 19,
  sm_starter_free: 26,
  upgrade_renewals_ar: 48
}
