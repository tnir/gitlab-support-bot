require 'fixtures/tickets'

module SharedContext
  extend RSpec::SharedContext

  let(:ticket_counts) { TICKET_COUNTS }
end

RSpec.configure do |config|
  config.before do
    SlackRubyBot.config.user = 'sb'

    config.include SharedContext
  end
end
