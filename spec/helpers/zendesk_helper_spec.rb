require 'spec_helper'

describe ZendeskHelper do
  vcr_search_options = {
    cassette_name: 'zendesk_search_api',
    record: :new_episodes,
    preserve_exact_body_bytes: true
  }

  vcr_view_options = {
    cassette_name: 'zendesk_view_api',
    record: :once,
    preserve_exact_body_bytes: true
  }

  def app
    ZendeskHelper
  end

  subject { app }

  context 'Views', vcr: vcr_view_options

  context 'Search' do
    context '#search_for', vcr: vcr_search_options do
      it 'is an array' do
        expect(search_for_geo.class).to eq(Array)
      end

      it 'ignores any closed and solved tickets' do
        statuses_we_dont_want = %w(on-hold closed solved)

        expect(search_for_geo.map(&:status).uniq).to_not include(statuses_we_dont_want)
        expect(search_for_git.map(&:status).uniq).to_not include(statuses_we_dont_want)
        expect(search_for_rack_attack.map(&:status).uniq).to_not include(statuses_we_dont_want)
      end

      it 'returns no more than the defined number of responses' do
        max_responses = ZendeskHelper::PER_PAGE

        expect(search_for_geo.count).to be <= max_responses
        expect(search_for_git.count).to be <= max_responses
        expect(search_for_rack_attack.count).to be <= max_responses
      end
    end

    context '#ticket_support_level' do
      it "returns N/A when the org doesn't exist" do
        # The following ticket belongs to GitLab and has no org
        # It's a closed ticket but that doesn't matter here
        result = app.ticket_support_level mock_ticket

        expect(result).to eq('N/A')
      end

      it 'returns the Support Level when it exists' do
        basic = app.ticket_support_level(mock_ticket('basic'))
        premium = app.ticket_support_level(mock_ticket('premium'))
        silver = app.ticket_support_level(mock_ticket('silver'))
        gold = app.ticket_support_level(mock_ticket('gold'))

        expect(basic).to eq('Basic')
        expect(premium).to eq('Premium')
        expect(silver).to eq('Silver')
        expect(gold).to eq('Gold')
      end
    end
  end

  private

  #                 _   _               _
  #  _ __ ___   ___| |_| |__   ___   __| |___
  # | '_ ` _ \ / _ \ __| '_ \ / _ \ / _` / __|
  # | | | | | |  __/ |_| | | | (_) | (_| \__ \
  # |_| |_| |_|\___|\__|_| |_|\___/ \__,_|___/
  def search_for_geo
    @search_for_geo ||= app.search_for('geo')
  end

  def search_for_git
    @search_for_git ||= app.search_for('git')
  end

  def search_for_rack_attack
    @search_for_rack_attack ||= app.search_for('rack attack')
  end

  def mock_ticket(support_level = nil)
    mock_ticket = ZendeskAPI::Ticket.new(app)
    return mock_ticket if support_level.nil?
    mock_ticket.organization = ZendeskAPI::Organization.new(app)
    mock_ticket.organization.organization_fields = { support_level: "#{support_level}" }
    mock_ticket
  end
end
