$LOAD_PATH.unshift(File.dirname(__FILE__))

require 'supportbot'

begin
  SupportBot::Bot.run
rescue StandardError => e
  STDERR.puts "ERROR: #{e}"
  STDERR.puts e.backtrace
  raise e
end

run SupportBot
