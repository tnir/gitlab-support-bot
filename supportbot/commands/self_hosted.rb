require_relative './base'

module SupportBot
  module Commands
    class SelfHosted < Base
      command 'self-hosted', 'sh', 'self-managed', 'sm'
      views :need_org_triage, :sm_premium_ultimate, :sm_starter_free
    end
  end
end
