require_relative './base'

module SupportBot
  module Commands
    class Services < Base
      command 'services', 's'
      views :need_org_triage, :dot_com_subscribers, :dot_com_trials, :dot_com_free
    end
  end
end
