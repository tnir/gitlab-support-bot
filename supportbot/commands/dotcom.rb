require_relative './base'

module SupportBot
  module Commands
    class Dotcom < Base
      command 'dotcom', 'dc'
      views :dot_com_subscribers, :dot_com_trials, :dot_com_free
    end
  end
end
