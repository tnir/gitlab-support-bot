require_relative './base'

module SupportBot
  module Commands
    class Pods < Base
      command 'pods', 'p'
      views :need_org_triage, :dot_com_subscribers, :dot_com_trials, :dot_com_free,
            :sm_premium_ultimate, :sm_starter_free, :upgrade_renewals_ar
    end
  end
end
