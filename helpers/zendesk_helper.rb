require 'zendesk_api'
require 'json'

module ZendeskHelper
  require_relative '../services/zendesk'
  PER_PAGE = 10

  class ZendeskView
    attr_reader :type, :name, :id

    def initialize(type, name, id)
      @type = type
      @name = name
      @id = id
    end
  end

  def self.count_many
    view_ids = views.keys.map { |v| views[v].id }.join(',')

    is_fresh = false
    while is_fresh == false
      fresh_arr = []
      output_hash = {}
      api_response = zd_client.view_counts(ids: view_ids, path: 'views/count_many', reload: true).fetch
      api_response.each do |json_views|
        fresh_arr.push(json_views['fresh'])
        zendesk_view_key = views.keys.find { |v| views[v].id == json_views['view_id'] }
        output_hash[zendesk_view_key] = json_views['value']
      end
      is_fresh = (fresh_arr.uniq == [true])
    end
    output_hash
  end

  def self.tickets_from_views
    ZendeskHelper.count_many
  end

  def self.views
    return @views unless @views.nil?

    @views = {}
    [
      [:need_org_triage, 'Needs Org & Triage', 360_062_019_613],
      [:dot_com_subscribers, 'GitLab.com Subscribers', 328_714_287],
      [:dot_com_trials, 'GitLab Trials', 360_062_361_674],
      [:dot_com_free, 'GitLab Free', 360_062_116_973],
      [:sm_premium_ultimate, 'Self-Managed Premium & Ultimate', 304_899_127],
      [:sm_starter_free, 'Self-Managed Starter & Free', 360_062_019_453],
      [:upgrade_renewals_ar, 'Upgrades, Renewals & AR', 360_034_975_053]
    ].each do |view_data|
      view = ZendeskView.new(*view_data)
      @views[view.type] = view
    end

    @views
  end

  def self.search_for(query, per_page = PER_PAGE)
    query = "#{query} type:ticket status:new status:open status:pending order_by:updated_at sort:desc"
    results = zd_client.search(query: query, per_page: per_page)
    results.to_a
  end

  def self.ticket_support_level(ticket)
    return 'N/A' if ticket.organization.nil?
    return 'N/A' unless ticket.organization.organization_fields[:support_level]
    ticket.organization.organization_fields[:support_level].try(:capitalize)
  end
end
