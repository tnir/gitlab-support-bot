module EmojiHelper
  def self.get_ticket_emoji(total)
    case total
    when 0
      "*#{total}* :yay:"
    when 1..11
      "*#{total}* :fistbump-connect:"
    when 11..22
      "*#{total}* :pizza:"
    when 22..44
      "*#{total}* :notbad:"
    when 44..75
      "*#{total}* :fire:"
    when 75..90
      "*#{total}* :killitwithfire:"
    else
      "*#{total}* :dumpster-fire:"
    end
  end
end
